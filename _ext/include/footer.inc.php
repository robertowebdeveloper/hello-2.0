<footer id="hl_footer">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-push-8 col-sm-4 col-sm-push-8 col-xs-12">
                    <div class="social">
                        <a href="https://www.facebook.com/Solgomma" class="sprite fb" target="_blank"></a>
                        <a href="https://www.youtube.com/channel/UCd3DX3m7u-Zvio9p6WqeCww" class="sprite yt" target="_blank"></a>
                        <a href="https://plus.google.com/u/0/101789798220183971196/about" class="sprite gp" target="_blank"></a>
                    </div>
                </div>
                <div class="col-md-8 col-md-pull-4 col-sm-8 col-sm-pull-4 col-xs-12">            
                    <div class="credits">
                        <p>
                            <b><# SOLGOMMA SPA #></b><br>
                            <# Solgomma SPA - 50053 Empoli (Firenze) Via Carraia, 29<br class="hidden-xs" />
                            Stabilimento e Uffici: Via Carraia, 31/43 - 0571 920067 - Ric.Aut. - fax: 0571 92 02 68 #>
                        </p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                	<p class="credits-inol3"><# design by #> <a href="http://www.inol3.com/" target="_blank"><# inol3 #></a></p>
				</div>
            </div>            
        </div>        
    </div>
</footer>