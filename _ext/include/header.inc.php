<header>
    <div class="header-site">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12"><a id="logo" href="{{url home}}"></a></div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="menu hidden-xs">
                        <ul>
                            <li><a href="{{url contatti}}"{{urlfired_class contatti}}>{{urlname contatti}}</a></li>
                            <li><a href="{{url press}}"{{urlfired_class press}}>{{urlname press}}</a></li>
                            <li><a href="{{url controlli-collaudi}}"{{urlfired_class controlli-collaudi}}>{{urlname controlli-collaudi}}</a></li>
                            <li class="bg-sub-menu-produzione">{{urlname produzione}}
                            	<ul class="sub-menu-produzione">
		                            <li><a href="{{url stampaggio-gomma}}"{{urlfired_class stampaggio-gomma}}>{{urlname stampaggio-gomma}}</a></li>
                                    <li><a href="{{url rivestimento-rulli}}"{{urlfired_class rivestimento-rulli}}>{{urlname rivestimento-rulli}}</a></li>
                                    <li><a href="{{url produzione-mescole}}"{{urlfired_class produzione-mescole}}>{{urlname produzione-mescole}}</a></li>
                            	</ul>
                            </li>
                            <li><a href="{{url azienda}}"{{urlfired_class azienda}}>{{urlname azienda}}</a></li>                                                            
                        </ul>
                    </div>                      
                    <div class="bg-menu-mobile visible-xs"> 
                        <div class="row">
                            <div class="col-xs-12">
                            	<a href="#" id="btnMenuMobile" onclick="openMobileMenu();"><img src="{{theme}}img/ico-menu-mobile.gif" width="20" class="pull-right" /></a>
                            </div>
                            <div class="col-xs-12">			
                            	<div id="menu-mobile">
                                    <ul>
                                        <li><a href="{{url azienda}}"{{urlfired_class azienda}}>{{urlname azienda}}</a></li>
                                        <li><a href="{{url stampaggio-gomma}}"{{urlfired_class stampaggio-gomma}}>{{urlname stampaggio-gomma}}</a></li>
    	                                <li><a href="{{url rivestimento-rulli}}"{{urlfired_class rivestimento-rulli}}>{{urlname rivestimento-rulli}}</a></li>
	                                    <li><a href="{{url produzione-mescole}}"{{urlfired_class produzione-mescole}}>{{urlname produzione-mescole}}</a></li>
                                        <li><a href="{{url controlli-collaudi}}"{{urlfired_class controlli-collaudi}}>{{urlname controlli-collaudi}}</a></li>
                                        <li><a href="{{url press}}"{{urlfired_class press}}>{{urlname press}}</a></li>
                                        <li><a href="{{url contatti}}"{{urlfired_class contatti}}>{{urlname contatti}}</a></li>
                                    </ul>
                            	</div>
                            </div>                        
                        </div>                        
                    </div>                               
                </div>                  
            </div>
        </div><!-- container -->
    </div><!-- header-site -->
</header>